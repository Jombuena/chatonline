// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true
};

export const firebaseConfig = {
  apiKey: 'AIzaSyBkM_jXB5lNbnGWSArpAIt-_2aQakgnVxs',
  authDomain: 'chatonline-2c011.firebaseapp.com',
  databaseURL: 'https://chatonline-2c011.firebaseio.com',
  projectId: 'chatonline-2c011',
  storageBucket: 'chatonline-2c011.appspot.com',
  messagingSenderId: '304228215145',
  appId: '1:304228215145:web:fd16f2d5bcbdb4539535f8',
  measurementId: 'G-JRXCJEQ8S8'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
