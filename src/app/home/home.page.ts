import {Component, OnInit} from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ChatsService, chat } from '../services/chats.service';
import { ActionSheetController, ModalController } from '@ionic/angular';
import { ChatComponent } from '../components/chat/chat.component';
import { User } from '../models/user';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit{

    public chatRooms: any = [];
    public userName: any = '';

    constructor(private auth: AuthService,
                public chatservice: ChatsService,
                private modalCtrl: ModalController,
                public actionSheetController: ActionSheetController) {}

    ngOnInit(){
        this.userName = this.auth.userEmail;
        console.log('home ' + this.userName);
          this.chatservice.getChatRooms().subscribe( chats => {
            this.chatRooms = chats;
        });
    }

    onLogOut(){
        this.auth.logout();
    }

    // tslint:disable-next-line:no-shadowed-variable
    openChatRoom(chat){
        this.modalCtrl.create({
            component: ChatComponent,
            componentProps: {
              chat: chat,
            }
        }).then((modal) => modal.present());
    }

    async presentActionSheet() {
        const actionSheet = await this.actionSheetController.create({
            header: 'Options',
            backdropDismiss: true,
            buttons: [{
                text: 'Logout',
                icon: 'close',
                handler: () => {
                    this.onLogOut();
                },
            }]
        });
        await actionSheet.present();
    }
}
