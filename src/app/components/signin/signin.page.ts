import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.page.html',
  styleUrls: ['./signin.page.scss'],
})
export class SigninPage implements OnInit {

  email: string;
  password: string;
  name: string;

  constructor(private auth: AuthService, public router: Router) { }

  ngOnInit() {
  }

  onSubmitSignin(){
    this.auth.register(this.email, this.password, this.name).then( res => {
      this.router.navigate(['/home']);
    }).catch(err => alert('Los datos son incorrectos o no existes'));
  }

}
