import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController} from '@ionic/angular';
import { Message } from '../../models/message';
import { ChatsService } from '../../services/chats.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit {

  public chat: any;

  public messages = [];
  public msg: string;
  public userName: string;
  public room: any;

  constructor(private navParams: NavParams,
              private modalCtrl: ModalController,
              private chatService: ChatsService) { }

  ngOnInit() {
    this.chatService.getChatRoom(this.chat.id).subscribe( room => {
      console.log(room);
      this.room = room;
    });

    this.chat = this.navParams.get('chat');

  }

  closeChat(){
    this.modalCtrl.dismiss();
  }

  sendMessage(){

    const message: Message = {
      content: this.msg,
      type: 'text',
      date: new Date()
    };

    if (this.msg != null){
      this.chatService.sendMsgToFirebase(message, this.chat.id);
      this.messages.push(this.msg);
      this.msg = '';
    } else {
      alert('Please Enter a Message');
    }

  }

}
