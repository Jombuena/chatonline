import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { promise } from 'protractor';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userName: any;
 public userEmail: string;

  constructor(private auth: AngularFireAuth, private router: Router, private db: AngularFirestore) { }

  login(email: string, pass: string){
    return new Promise((resolve, rejected) => {
      this.auth.signInWithEmailAndPassword(email, pass).then(user => {
        resolve(user);
        // this.userEmail = user.user.email;
        // this.getUserName();
      }).catch(err => rejected(err));
    });

  }

  register(email: string, pass: string, name: string){
    return new Promise ((resolve, rejected) => {
     this.auth.createUserWithEmailAndPassword(email, pass).then(res => {
       const uid = res.user.uid;
       this.db.collection('users').doc(uid).set({
         name,
         uid
       });
       resolve(res);
       this.getUserName();
     }).catch(err => rejected(err));
    });
  }


  getUserName(){
    console.log(this.userName);
    return this.userName;
  }

  logout(){
    this.auth.signOut().then(() => {
      this.router.navigate(['/login']);
    });
  }

}
