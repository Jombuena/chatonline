import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import {isNotNullOrUndefined} from 'codelyzer/util/isNotNullOrUndefined';
@Injectable({
  providedIn: 'root'
})
export class NologinGuard implements CanActivate {

  constructor(private fireAuth: AngularFireAuth, public router: Router) { }


  canActivate(
      next: ActivatedRouteSnapshot,
      state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    return this.fireAuth.authState.pipe(map(auth => {

      if (!isNotNullOrUndefined(auth)) {
        return true;
      } else {
        this.router.navigate(['/home']);
        return false;
      }
    }));

  }
}
